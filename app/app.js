(function () {
    app.initialized()
        .then(function (_client) {
            let client = _client;
            client.events.on('app.activated', modalListener(client));
            client.instance.receive(event => { dataHandler(client, event) });
            client.data.get('ticket')
                .then(function (ticket) {
                    let iparamsPromise = client.iparams.get();
                    let issueMappingPromise = client.db.get(`ticket:${ticket.ticket.id}`);
                    Promise.all([iparamsPromise, issueMappingPromise])
                        .then (function(promisesData) {
                            const iparams = promisesData[0];
                            const mappingData = promisesData[1]
                            const githubIssueUrl = `https://github.com/${iparams.githubAccountName}/${iparams.githubRepoName}/issues/${mappingData.value}`;
                            $("#showIssueLink").attr("href", githubIssueUrl);
                            $("#showIssueLink").show();
                            $("#showModal").hide();
                        },
                        function(error) {
                            // Didn't find the mapping
                            console.log("Error occurred while retriving ticket-issue mapping", error);
                            $("#showIssueLink").hide();
                            $("#showModal").show();
                        });
                })
                .catch(err=>{
                    console.log("Fetching ticket details failed", err);
                    return null;
                });
        })
        .catch(err=>{
            console.log("App initialization failed", err);
            return null;
        });
})();

function modalListener(client) {
    $('#showModal').click(
        function () {
            client.data.get('ticket')
                .then(function (ticket) {
                    client.interface.trigger("showModal", {
                        title: "Github Issue Details",
                        template: "modal.html",
                        data: ticket,
                    })
                })
                .catch(err=>{
                    console.log("Fetching ticket details failed", err);
                    return null;
                });
        });
}

function dataHandler(client, event) {
    let message = {
        type: 'danger',
        title: "Failed",
        message: "Oops issue creation failed"
    }
    let data = event.helper.getData().message;
    if (!jQuery.isEmptyObject(data)) {
        client.request.invoke("createGithubIssue", data).then(
            function (res) {
                let response = res.response;
                if (response.message) {
                    message["type"] = "success";
                    message["title"] = "Success"
                    message["message"] = response.message;
                    $("#showIssueLink").attr("href", response.githubIssueUrl);
                    $("#showIssueLink").show();
                    $("#showModal").hide();
                }
                errorHandler(client, message);
            },
            function (error) {
                console.log("Error: ", error);
                errorHandler(client, message);
            })
            .catch(function (e) {
                console.log('Exception: ', e);
                errorHandler(client, message);
            });
    } else {
        errorHandler(client, message);
    }
}

function errorHandler(client, data) {
    client.interface.trigger("showNotify", {
        type: data.type, title: data.title,
        message: data.message
    })
}




