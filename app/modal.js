var data = {};
var client = null
$(document).ready(function () {
    app.initialized()
        .then((_client) => {
            client = _client;
            client.instance.context()
                .then(
                    (context) => {
                        data = context.data;
                        $("input[name='subject']").val(data.ticket.subject);
                        $("input[name='description']").val(data.ticket.description_text);
                        data.ticket['type'] = $("#issueType").val();
                    }
                )
                .catch((err)=>{
                    console.log("Error occurred in retriving context", err);
                });
        })
        .catch((err)=>{
            console.log("Error occurred in app initialization", err);
        });
});

function changeType() {
    data.ticket['type'] = $("#issueType").val();
}
function onCreateIssue() {
    closeIt();
}

function onCancleIssue() {
    data = {}
    closeIt();
}

function closeIt() {
    client.instance.send({ message: data });
    client.instance.close();
}