## Freshdesk App Project

Congratulations on creating your App Project! Feel free to replace this text with your project description.

### Project folder structure explained

    .
    ├── README.md                  This file.
    ├── config                     Installation parameter configs.
    │   ├── iparams.html           Installation parameter config UI
    │   └── iparam_test_data.json  Installation parameter data for local testing.
    └── manifest.json              Project manifest.
    └── server                     Business logic for remote request and event handlers.
        ├── lib
        │   └── handle-response.js
        ├── server.js // Main entry point for all the events
        ├── helpers
        │   │── Mapping.service.js // takes care of data storage, currently handles storage and retrieval of data from Freshdesk store
        │   ├── Constants.js // global constants
        │   ├── Freshdesk.service.js // Service takes care of all interaction with Freshdesk V2 apis
        │   ├── Github.service.js // Services handles all interaction with Github apis
        │   ├── FreshdeskGithub.service.js // Service takes care of transformation logic from Freshdesk to Github
        │   └── GithubFreshdesk.service.js // Service takes care of transformation logic from Github to Freshdesk
        └── test_data
            ├── onAppInstall.json
            ├── onAppUninstall.json
            ├── onContactCreate.json
            ├── onContactUpdate.json
            ├── onConversationCreate.json
            ├── onExternalEvent.json
            ├── onTicketCreate.json
            └── onTicketUpdate.json
