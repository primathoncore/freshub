// npm packages
const DataTransform = require("node-json-transform").DataTransform;
const octonode = require('octonode');

//Helper services
const GithubService = require("./Github.service")
const FreshdeskService = require("./Freshdesk.service")
const MappingService = require('./Mapping.service');


// local constants
const freshdeskToGithubMap = {
	"title": "subject",
	"body": "description_text",
	"labels": "type"
};

let FreshdeskGithubService = {
	
	/**
	 * helper function to create json transformer map for DataTransform module
	 */
	freshdeskTicketToGithubIssueMap: function(){
		return {
		    list : '',
		    item: freshdeskToGithubMap,
		    operate: [
		        {
		            run: function(val) { return [val] ; }, on: "labels"
		        }  
		    ],
		    each: function(item){
		      return item;
		    }
		};
	},

	/**
	 * helper function to transform Freshdesk payload to github payload
	 */
	getIssuePayload: function(payload){
		let issueMap = FreshdeskGithubService.freshdeskTicketToGithubIssueMap(); 
		let dataTransform = DataTransform([payload], issueMap);
		let issuePayload = dataTransform.transform()[0];
		return issuePayload;
	},

	/**
	 * Function to create issue in Github from Freshdesk payload
	 */
	createIssue: function(payload){
		return new Promise((resolve, reject) => {
			if (!(payload.ticket && payload.iparams)) 
				return reject({"message": "ticket and/or iparams not found"});
			let ticket = payload.ticket;
			let iparams = payload.iparams;
			MappingService.getGithubIssueId(ticket.id)
				.then(issueId => {
						if(!issueId){
							let githubIssuePayload = FreshdeskGithubService.getIssuePayload(ticket);
							let repoAddress = GithubService.createRepoAddress(iparams.githubAccountName, iparams.githubRepoName);
							let gclient = octonode.client(iparams.githubApiKey);
							GithubService.createIssue(gclient, repoAddress, githubIssuePayload)
							.then(res => {
								if(res && res.number && res.html_url){
									try {
										let issueNumber = res.number;
										MappingService.createGithubFreshdeskMapping(issueNumber, ticket.id);
										MappingService.createFreshdeskGithubMapping(ticket.id, issueNumber);
									} catch (err) {
										console.log("Error occurred while updating ticket custom fields", err)
									}
								}
								return resolve({"message": "Issue Created Successfully", "githubIssueUrl": res.html_url});
							})
							.catch((err) => {
								console.log('error', err);
								reject(err)
							});
						}else{
							return resolve({"message": "Issue already exist"});
						}
				})
				.catch((err) => {
					console.log('error creating issue', err);
					reject(err);
				})
		})
	},

	/**
	 * Function to close issue in Github on basis of freshdesk ticket details
	 */
	closeIssue: function(iparams, ticket){
		return new Promise((resolve, reject) => {
			console.log("closeIssue", ticket)
			MappingService.getGithubIssueId(ticket.id)
				.then(issueId => {
					if(issueId){
						let updateMap = {'state': 'closed'};
						let repoAddress = GithubService.createRepoAddress(iparams.githubAccountName, iparams.githubRepoName);
						let gclient = octonode.client(iparams.githubApiKey);
						console.log('githubService.editIssue: ', repoAddress, issueId, updateMap);
						GithubService.editIssue(gclient, repoAddress, issueId, updateMap)
							.then(data => {
								resolve(data);  
							})
							.catch((err) => {
								console.log('error closing issue', err);
								reject(err);
							})
					}else{
						resolve(true);
					}
				})
				.catch((err) => {
					console.log('error closing issue', err);
					reject(err);
				})
		});
	},

	/**
	 * Function to add comment in Github when replied in Freshdesk
	 */
	addComment : function(ticket, iparams, comment){
		return new Promise((resolve, reject) =>{
			if(comment){
				// let issueId = this.getGithubIssueNumber(ticket, iparams.githubRefCustomField);
				MappingService.getGithubIssueId(ticket.id)
					.then(issueId => {
						console.log("IssueId", issueId);
						if(issueId){
							let repoAddress = GithubService.createRepoAddress(iparams.githubAccountName, iparams.githubRepoName);
							let gclient = octonode.client(iparams.githubApiKey);
							GithubService.addComment(gclient, repoAddress, issueId, comment)
								.then(data => {
									if(Array.isArray(data) && data.length > 0 && data[0].id){
										MappingService.markGithubCommentSync(data[0].id)
											.then(()=>{
												resolve(data);  
											})
											.catch((err) => {
												console.log('Error occurred while marking github comment true', err);
												reject(err);
											})
									}else{
										resolve(data);  
									}
									
								})
								.catch((err) => {
									console.log('Error occurred while adding comment', err);
									reject(err);
								})
						}else{
							resolve(true);
						}
					})
					.catch((err) => {
						console.log('error adding comment on issue', err);
						reject(err);
					})
			}else{
				resolve(true);
			}
		})
	},

	/**
	 * Function to create webhook in github basis iparams
	 */
	createGithubWebhook: function(payload, url){
		return new Promise((resolve, reject) =>{
			if(payload && payload.iparams && url){
				GithubService.createWebhook(payload.iparams, url)
					.then(data => {
						resolve(data);  
					})
					.catch((err) => {
						console.log('Error occurred while adding comment', err);
						reject(err);
					})
			}else{
				reject(new Error("Invalid payload or url"));
			}
		});
	},
};
exports = FreshdeskGithubService;