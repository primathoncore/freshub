
const FreshdeskService = require('./Freshdesk.service');
const Constants = require("./Constants")
const MappingService = require('./Mapping.service');
/**
 * A set of helper functions to take action on Freshdesk ticket as per events from Github
 */
let GithubFreshdeskService = {
    
    addFreshDeskNote: function(iparams, ticketId, body){
        if(iparams && body){
            let apiConfig = FreshdeskService.getApiConfig(iparams);
            return FreshdeskService.addNote(apiConfig, ticketId, body, isPrivate = false, attachments = [])
        }
        return null;
    },

    closeTicket: function(iparams, ticketId){
        if(iparams){
            let updateObject = {"status": Constants.freshdesk.statusClosed} // // represents closed
            let apiConfig = FreshdeskService.getApiConfig(iparams);
            return FreshdeskService.updateTicket(apiConfig, ticketId, updateObject);
        }
        return null;
    },

    _markFreshdeskConvoSync: function(data) {
        return MappingService.markFreshdeskConvoSync(data.id)
        .then((createdData) => {
            console.log("Created commented entry in Data store", createdData);
        })
        .catch((err) => {
            console.log('Error occrred while store Github comment id', err)
        })
    },

    _addFreshDeskNote: function(iparams, ticketId, githubPayload) {
        return GithubFreshdeskService.addFreshDeskNote(iparams, ticketId, githubPayload.comment.body)
        .then(data => {
            console.log('onExternalEventHandler note creation successful', data)
            return GithubFreshdeskService._markFreshdeskConvoSync(data);
        })
        .catch((err) => {
            console.log('Error occurred in onExternalEventHandler', err);
        })
    },

    syncCommentFromGithub: function(iparams, githubPayload, ticketId) {
        return MappingService.checkGithubCommentId(githubPayload.comment.id)
        .then(storedData => {
            if(!storedData) {
                return GithubFreshdeskService._addFreshDeskNote(iparams, ticketId, githubPayload)
            } else {
                console.log(`${githubPayload.comment.id} is already synced`);
            }
        })
        .catch(err => {
            console.log('Error occurred in onExternalEventHandler while fetching github comment id', err);
        });
    }
}

exports = GithubFreshdeskService;