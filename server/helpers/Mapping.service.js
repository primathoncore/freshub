//local constants
const GITHUB_KEY_PREFIX = "issue";
const FRESHDESK_KEY_PREFIX = "ticket";
const FRESHDESK_CONVO_KEY_PREFIX = "conversation";
const GITHUB_COMMENT_KEY_PREFIX = "comment";
const GITHUB_WEBHOOK_KEY = "githubWebhookId";

let MappingService = {

	/**
	 * Function to create mapping between Github issue number and Freshdesk ticket id
	 */
	createMapping: function(key, value){
		return new Promise((resolve, reject) =>{
			$db.set(key, {"value": value})
			.done((data) =>{
				return resolve(data);
			})
			.fail((err) => {
				console.log('error creating mapping', err);
				return reject(false);
			});
		})
	},

	/**
	 * Get value stored in DB by key. If doesn't exist, it returns null.
	 */
	getValueByKey : function(key){
		console.log("getValueByKey", key);
		return new Promise((resolve, reject) => {
			$db.get(key)
				.done( data => {
					console.log("Mapping fetch for key", key, "value", data)
					return resolve(data.value);
				})
				.fail(err => {
					if(err.status == 404){
						return resolve(null);
					}else{
						console.log('Error occurred while fetching value for key', key, err);
						return reject(err);
					}
				});
		})
	},

	/**
	 * Function to delete a key and its data
	 */
	deleteMapping: function(key){
		return new Promise((resolve, reject) =>{
			$db.delete(key)
			.done(() =>{
				return resolve();
			})
			.fail((err) => {
				if(err.status == 404){
					return resolve();
				}else{
					console.log('Error deleting mapping', err);
					return reject(err);
				}
			});
		})
	},

	createGithubFreshdeskMapping: function(issueId, ticketId){
		return MappingService.createMapping(`${GITHUB_KEY_PREFIX}:${issueId}`, ticketId);
	},

	createFreshdeskGithubMapping: function(ticketId, issueId){
		return MappingService.createMapping(`${FRESHDESK_KEY_PREFIX}:${ticketId}`, issueId);
	},

	/**
	 * Function to retrieve freshdesk ticket by issue id
	 */
	getFreshdeskTicketId : function(issueId){
		return MappingService.getValueByKey(`${GITHUB_KEY_PREFIX}:${issueId}`)
	},

	getGithubIssueId : function(ticketId){
		return MappingService.getValueByKey(`${FRESHDESK_KEY_PREFIX}:${ticketId}`)
	},

	checkFreshdeskConvoId: function(conversationId){
		return MappingService.getValueByKey(`${FRESHDESK_CONVO_KEY_PREFIX}:${conversationId}`)
	},

	checkGithubCommentId: function(commentId){
		return MappingService.getValueByKey(`${GITHUB_COMMENT_KEY_PREFIX}:${commentId}`)
	},

	markFreshdeskConvoSync: function(conversationId){
		return MappingService.createMapping(`${FRESHDESK_CONVO_KEY_PREFIX}:${conversationId}`, true);
	},

	markGithubCommentSync: function(commentId){
		return MappingService.createMapping(`${GITHUB_COMMENT_KEY_PREFIX}:${commentId}`, true);
	},

	createGithubWebhookMapping: function(webhookId){
		return MappingService.createMapping(`${GITHUB_WEBHOOK_KEY}`, webhookId);
	},

	getGithubWebhookMaping : function(){
		return MappingService.getValueByKey(`${GITHUB_WEBHOOK_KEY}`)
	},

	removeGithubWebhookMaping : function(){
		return MappingService.deleteMapping(`${GITHUB_WEBHOOK_KEY}`)
	},


}


exports = MappingService;