const octonode = require('octonode');

let GithubIssueService = {

    /**
     * Validate configuration settings
     */
    validateConfigs: function(iparams){
        return new Promise((resolve, reject) =>{
            let githubClient = octonode.client(iparams.githubApiKey);
            githubClient.infoAsync()
                .then(data=>{
                    console.log("Client info:", data);
                    return resolve();
                })
                .catch(err=>{
                    console.log("Error occurred while validating client configs", err)
                    return reject(err);
                })
        });
    },

    /**
     * Function to get issue details
     */
    getIssue :  function (client, repoAddress, issueNumber){
        let issue = client.issue(repoAddress, issueNumber);
        return issue.infoAsync();
    },

    /**
     * Helper function to create github repo address
     */
    createRepoAddress : function (username, repoName){
        if (username && repoName){
            let repoAddress = `${username}/${repoName}`;
            return repoAddress;
        } 
        console.log('unable to create createRepoAddress using username: ', username, 'and repoName :', repoName);
        return null;
    },
    
    /**
     * Creates an issue for a github repository
     */
    createIssue :  function (client, repoAddress, issueData) {
        return new Promise((resolve, reject) =>{
            let repo = client.repo(repoAddress);
            repo.issue(issueData, (err, data) => {
                if(err){
                    console.log('err', err);
                    reject(err);
                }
                resolve(data);
            })
        }); 
    },

    /**
     * Function to modify an issue of a particular github repository
     */
    editIssue :  function (client, repoAddress, issueNumber, editDataMap){
        return new Promise((resolve, reject) =>{
            let issue = client.issue(repoAddress, issueNumber);
            let issueDataPromise = this.getIssue(client, repoAddress, issueNumber);
            issueDataPromise
            .then(currentData =>{
                let issueEditKeys = ["title", "body", "state", "labels", "assignees", "milestone"];
                let issueData = {};
                for(issueEditKay of issueEditKeys){
                    if(currentData[issueEditKay]){
                        issueData[issueEditKay] = currentData[issueEditKay]
                    }
                }
                for (key of Object.keys(editDataMap)){
                    issueData[key] = editDataMap[key];
                }
                console.log('issueData', issueData);
                issue.updateAsync(issueData)
                    .then(data => {
                        resolve(data);  
                    })
                    .catch((err) => {
                        console.log('error updating issue', err);
                        reject(err);
                    })   
            })
            .catch((err) => {
                console.log('error', err);
                reject(err)
            })
        })
    },

    /**
     * Function to get issue details
     */
    getIssue :  function (client, repoAddress, issueNumber){
        let issue = client.issue(repoAddress, issueNumber);
        return issue.infoAsync();
    },

    /**
     * Function to add comment to a github issue
     */
    addComment: function(client, repoAddress, issueNumber, comment){
        return new Promise((resolve, reject) =>{
            let issue = client.issue(repoAddress, issueNumber);
            issue.createCommentAsync({'body': comment})
            .then((data) =>{
                resolve(data);
            })
            .catch((err) => {
                console.log('error adding comment',repoAddress, issueNumber, comment, "err :", err);
                reject(err);
            })
        });
    },

    /**
     * Function to configure webhook for a github repository
     */
    createWebhook: function(iparams, url , events = []){
        return new Promise((resolve, reject) =>{
            if(url && iparams){
                let repoAddress = this.createRepoAddress(iparams.githubAccountName, iparams.githubRepoName)
                let gclient = octonode.client(iparams.githubApiKey);
                let repoObject = gclient.repo(repoAddress);
                let hookData = {
                    "name": "web",
                    "active": true,
                    "events": ["issue_comment", "issues"],
                    "config": {
                        "url": url,
                        "content_type": "json"
                    }
                }
                if(events && Array.isArray(events) && events.length > 0){
                    hookData["events"] = events
                }
                repoObject.hook(hookData, (err, res) => {
                    if (err){
                        console.log('error in creating hook', err);
                        reject(err)
                    } 
                    resolve(res)
                });
            }else{
                reject(new Error("Invalid Params"));
            }
        });
    },

    removeGithubWebhook: function(iparams, webhookId){
        return new Promise((resolve, reject) =>{
            let repoAddress = this.createRepoAddress(iparams.githubAccountName, iparams.githubRepoName)
            let gclient = octonode.client(iparams.githubApiKey);
            let repoObject = gclient.repo(repoAddress);
            repoObject.deleteHook(webhookId, (err, res) => {
                if (err){
                    console.log('Error in removing hook', err);
                    return reject(err)
                } 
                return resolve(res)
            });
        });
    }
}


exports = GithubIssueService;