//npm packages
const request = require('request');
const Buffer = require('buffer').Buffer

let FreshdeskService = {
    
    /**
     * Function to generate api token from api key
     */
    getAuthHeaders: function(apiKey){
        let auth = "Basic " + Buffer.from(apiKey + ":" + 'X').toString("base64");
        return auth;
    },

    /** 
     * Function to make api request to Freshdesk V2 apis. Support JSON response currently
     */
    makeRequest: function (method, uri, apiConfig, qs, data) {
        const authToken = FreshdeskService.getAuthHeaders(apiConfig.apiKey)
        const url = `${apiConfig.serverName}${uri}`;
        return new Promise(function(resolve, reject){
            const options = {
                'method': method,
                'headers': {
                    'Content-Type': 'application/json',
                    'Authorization': authToken
                },
                'url': url,
                'qs': qs
            }
            if(data) {
                options.body = JSON.stringify(data)
            }
            request(options, function(error, response, body){
                if (error) {
                    return reject(error);
                }
                if(response.statusCode>=300){
                    return reject(body);
                }
                return resolve(JSON.parse(body)); // assuming response would always be JSON
            });
        });
    },

    /** 
     * Function to retrieve ticket details from Freshdesk
     */    
    getTicket:  function(apiConfig, ticketId){
        return new Promise((resolve, reject) => {
            FreshdeskService.makeRequest("GET", `/api/v2/tickets/${ticketId}`, apiConfig, null, null)
                .then((data) => {
                    return resolve(data);
                })
                .catch((err) => {
                    console.log(`Error occurred while fetching Freshdesk ticket: ${ticketId}\n`, err);
                    return reject(null);
                })
        })
    },
  
    /** 
     * Function to update ticket fields in Freshdesk
     */
    updateTicket : function(apiConfig, ticketId, updateObject){
        return new Promise((resolve, reject) => {
            FreshdeskService.makeRequest("PUT", `/api/v2/tickets/${ticketId}`, apiConfig, null, updateObject)
                .then((data) => {
                    console.log('Ticket updated successfully', data);
                    return resolve(true);
                })
                .catch((err) => {
                    console.log('error updating ticket', err);
                    return reject(false);
                });
        });
    },

    /**
     * Helper function to pass on only Freshdesk API configs from iparams
     */
	getApiConfig : function(iparams){
		if (!(iparams && iparams.subdomain && iparams.apiKey)){
			console.log('Invalid iparams', iparams);
			return null;
		}
		return {
			"serverName": `https://${iparams.subdomain}.freshdesk.com`,
			"apiKey": iparams.apiKey
		}
	},
  
    /** 
     * Function to add a public/private note or reply/forward conversation
     */
    addNote : function(apiConfig, ticketId, body, isPrivate = false){ // doesn't support attachments currently
        return new Promise(function(resolve, reject){
			try{
				let payload = {
                    "body": body,
                    "private": isPrivate,
                    "incoming": true
                };
                FreshdeskService.makeRequest("POST", `/api/v2/tickets/${ticketId}/notes`, apiConfig, null, payload)
                    .then((data) => {
                        console.log('Note added successfully', data);
                        return resolve(data);
                    })
                    .catch((err) => {
                        console.log('Error occurred while adding note', err);
                        return reject(err);
                    });
			}catch(err){
				console.log("Freshdesk Error", `Error occurred while adding note to Freshdesk ticket: ${ticketId}`, "payload", body, "Error: ", error);
				return reject(err);
			}
		});
    },

	/**
     * Validate configuration settings
     */
    validateConfigs: function(iparams){
        let apiConfig = FreshdeskService.getApiConfig(iparams);
        return new Promise((resolve, reject) => {
            FreshdeskService.makeRequest("GET", `/api/v2/settings/helpdesk`, apiConfig, null, null)
                .then((data) => {
                    console.log("Freshdesk Token works fine", data);
                    return resolve();
                })
                .catch((err) => {
                    console.log(`Error occurred while fetching Freshdesk settings:`, err);
                    return reject(err);
                })
        })
    },

};

exports = FreshdeskService;





