// services
const FreshdeskGithubService = require('./helpers/FreshdeskGithub.service');
const GithubFreshdeskService = require('./helpers/GithubFreshdesk.service');
const FreshdeskService = require('./helpers/Freshdesk.service');
const GithubService = require('./helpers/Github.service');
const MappingService = require('./helpers/Mapping.service')
const Constants = require('./helpers/Constants')

exports = {
    events: [
        { event: "onAppInstall", callback: "createHook" },
        { event: "onAppUninstall", callback: "unhookGithub" },
        { event: "onTicketUpdate", callback: "issueCloseHandler" },
        { event: "onConversationCreate", callback: "addCommentHandler" },
        { event: "onExternalEvent", callback: "onExternalEventHandler" }
    ],

    createHook: function(payload) {
        FreshdeskService.validateConfigs(payload.iparams)
            .then(()=>{
                generateTargetUrl()
                .then(function(url) {
                    FreshdeskGithubService.createGithubWebhook(payload, url)
                        .then(data=>{
                            MappingService.createGithubWebhookMapping(data.id)
                                .then(()=>{
                                    renderData();
                                })
                                .catch(err=>{
                                    console.log('[Installation Error] occurred while creating webhook id entry in DB', err);
                                    renderData({message: "App installation failed. Please contact support team!"});
                                })
                        })
                        .catch(err=>{
                            console.log('[Installation Error] occurred while creating webhook on Github', err);
                            renderData({message: "App installation failed. Please recheck github configurations or contact support team"});
                        });
                })
                .fail(function(err) {
                    console.log('[Installation] Failure generating github webhook url', err);
                    renderData({message: "App installation failed. Please contact support team"});
                });
            })
            .catch(err=>{
                    console.log('[Installation] Failure validating Freshdesk configs', err);
                    renderData({message: "Invalid Freshdesk configs"});
            });
    },

    unhookGithub: function(payload){
        MappingService.getGithubWebhookMaping()
            .then(webhookId=>{
                GithubService.removeGithubWebhook(payload.iparams, webhookId)
                    .then(()=>{
                        MappingService.removeGithubWebhookMaping()
                            .then(()=>{
                                renderData();
                            })
                            .catch(err=>{
                                console.log('[Installation] Failure remove data from DB', err);
                                renderData({message: "Oops! Some error occurred while uninstalling app, please try again later!"});
                            })
                    })
                    .catch(err=>{
                        console.log('[Installation] Failure uninstalling app', err);
                        renderData({message: "Oops! Some error occurred while uninstalling app, please try again later!"});
                    });
            })
            .catch(err=>{
                console.log('[Installation] Failure uninstalling app', err);
                renderData({message: "Webhook doesn't exist on github. No worries!"});
            })
        
    },

    onExternalEventHandler: function(payloadMain){
        if(payloadMain && payloadMain.data){
            const githubPayload = payloadMain.data;
            const iparams = payloadMain.iparams;
            console.log("githubPayload.issue.number", githubPayload.issue.number);
            if(githubPayload && githubPayload.action == "closed" || githubPayload.action == "created"){
                let ticketIdPromise = MappingService.getFreshdeskTicketId(githubPayload.issue.number);
                ticketIdPromise
                .then((ticketId) => {
                    if(ticketId){
                        switch(githubPayload.action) {
                            case Constants.github.actionClosed:{
                                GithubFreshdeskService.closeTicket(iparams, ticketId)
                                .then(data => {
                                    console.log('onExternalEventHandler ticket closure successful', data)
                                })
                                .catch((err) => {
                                    console.log('Error occurred in onExternalEventHandler', err);
                                })
                                break;
                            }
                            case Constants.github.actionCreated:
                                if(githubPayload.comment && githubPayload.comment.body){
                                    GithubFreshdeskService.syncCommentFromGithub(iparams, githubPayload, ticketId)
                                        .then(data => {
                                            console.log("Comment successfully synced from Github to Freshdesk", data);
                                        })
                                        .catch(err=>{
                                            console.log('Error occurred in onExternalEventHandler while fetching github comment id', err);
                                        });                                    
                                }
                                break;
                            default:
                                // pass
                        }
                    }
                })
                .catch((err) => {
                    console.log('error getting ticketId for error:', err);
                });
            }
        }
    },

    createGithubIssue: function(payload){
        FreshdeskGithubService.createIssue(payload)
        .then(data => {
                console.log('Github issue creation successful', data);
                renderData(null,  data);
        })
        .catch((err) => {
            console.log('createGithubIssue error occurred', err)
            renderData(null,    '');
        })
    },

    issueCloseHandler: function(payload){
        if(payload && payload.data && payload.data.ticket){
            let ticket = payload.data.ticket;
            let iparams = payload.iparams;
            console.log("Ticket, ", ticket, ticket.status , Constants.freshdesk.statusClosed);
            if(ticket.status == Constants.freshdesk.statusClosed){
                console.log('closing ticket', ticket.id);
                FreshdeskGithubService.closeIssue(iparams, ticket)
                    .then((data) => {
                        console.log('Issue closed successfully, ticketId', data, ticket.id)
                    })
                    .catch((err) => {
                        console.log('Error occurred in closing issue', err);
                    })
            }else{
                console.log('Ignorning non-closure events');
            }
        }else{
            console.log("Issueclosehandler invalid payload", payload);
        }
    },

    addCommentHandler : function(payload){
        if(payload && payload.data && payload.data.conversation && payload.iparams && (! payload.data.conversation.private)){
            let conversation = payload.data.conversation;
            let apiConfig = FreshdeskService.getApiConfig(payload.iparams)
            let ticketPromise = FreshdeskService.getTicket( apiConfig ,conversation.ticket_id)
            ticketPromise
                .then((ticket) =>{
                    MappingService.checkFreshdeskConvoId(conversation.id)
                    .then((storedData)=>{
                        if(!storedData){
                            FreshdeskGithubService.addComment(ticket, payload.iparams, conversation.body_text)
                            .then(response => {
                                console.log("Added comment response", response);
                            })
                            .catch(err=>{
                                console.log("Error occurred while creating comment on github", err);
                            });
                        }else{
                            console.log(`${conversation.id} is already synced`);
                        }
                    })
                    .catch(err=>{
                        console.log("Error occurred while retrieving Handler", err);
                    });
                    
                })
                .catch(err=>{
                    console.log("Error occurred in addComment Handler", err);
                })
        }
    },

    
};